#pragma once
#include "Item.h"
#include "SFML\Graphics.hpp"

using namespace std;

class DynamicItem :
	public Item
{
public:
	DynamicItem();
	~DynamicItem();
protected:
	sf::Sprite _sprite;
};

