#pragma once
#include <map>
#include "Entity.h"

using namespace std;

class EntityList
{
public:
	EntityList();
	const map<unsigned int, Entity*>& getMap() const;
	~EntityList();
private:
	map<unsigned int, Entity*> _entities;
};

