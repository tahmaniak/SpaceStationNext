#include "FPSCounter.h"
#include "iostream";


FPSCounter::FPSCounter()
{
	sf::Font font;
	font.loadFromFile("C:/Windows/Fonts/arial.ttf");
	_text.setFont(font);
	_text.setCharacterSize(24);
	_text.setFillColor(sf::Color::White);
	_clock.restart();
	_frameCount = 0;
}

FPSCounter::~FPSCounter()
{
}

void FPSCounter::update() {
	_frameCount++;

	if (_clock.getElapsedTime().asMilliseconds() >= 1000) {
		std::cout << _frameCount << endl;
		_text.setString(to_string(_frameCount));
		_frameCount = 0;
		_clock.restart();

	}
}

void FPSCounter::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(_text);
}
