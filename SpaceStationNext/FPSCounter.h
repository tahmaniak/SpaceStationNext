#pragma once

#include "SFML/Graphics.hpp"

using namespace std;

class FPSCounter :
	public sf::Drawable
{
private:
	sf::Clock _clock;
	sf::Time _lastTime;
	sf::Text _text;
	unsigned int _frameCount;
public:
	FPSCounter();
	~FPSCounter();

	void update();

	// H�rit� via Drawable
	virtual void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
};

