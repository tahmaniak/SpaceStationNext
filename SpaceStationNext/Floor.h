#pragma once
#include "StaticItem.h"


class Floor :
	public StaticItem
{
public:
	Floor();
	static ItemRegister<Floor> reg;
	~Floor();
};

