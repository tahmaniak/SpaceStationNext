#include "Game.h"



Game::Game()
{
}

inline void Game::mainLoop()
{
	
	// Event processing
	sf::Event event;
	while (_window.pollEvent(event))
	{
		// Request for closing the window
		if (event.type == sf::Event::Closed)
			_window.close();

		if (event.type == sf::Event::KeyPressed) {
			float speed = 8.f;
			switch (event.key.code)
			{
			case sf::Keyboard::D:
				_view.move(speed, 0.f);
				break;
			case sf::Keyboard::A:
				_view.move(-speed, 0.f);
				break;
			case sf::Keyboard::W:
				_view.move(0.f, -speed);
				break;
			case sf::Keyboard::S:
				_view.move(0.f, speed);
				break;
			default:
				break;
			}
		}


	}

	_window.clear(sf::Color(16, 26, 43));

	_window.setView(_view);

	_fps.update();
	World::getInstance().update();

	_window.draw(World::getInstance());

	_window.display();
}

void Game::initialize()
{
	_window.create(sf::VideoMode(1920, 1080), "Space Station: Next [0.0.0]");
	_window.setFramerateLimit(400);
	_window.setVerticalSyncEnabled(false);

	try {
		Itemmanager::getInstance().initialize();
	}
	catch (exception e) {
		string what(e.what());
		cout << "Error initializing item manager (" + what + ")" << endl;
		system("pause");
		exit(EXIT_FAILURE);
	}

	_view = _window.getView();
	_view.zoom(0.33f);
	_view.setCenter(sf::Vector2f());

	World::getInstance().setView(_view);

	string mapName = "default";

	try {
		World::getInstance().load(mapName);
	}
	catch (exception e) {
		string what(e.what());
		cout << "Error loading map \""+mapName+"\" (" + what + ")" << endl;
		system("pause");
		exit(EXIT_FAILURE);
	}
	

	while (_window.isOpen()) {
		mainLoop();
	}

	
}


Game::~Game()
{
}
