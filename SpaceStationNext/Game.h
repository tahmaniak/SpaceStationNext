#pragma once
#include "SFML\Graphics.hpp"
#include "Itemmanager.h"
#include "World.h"
#include "FPSCounter.h"

class Game
{
public:
	Game();
	void inline mainLoop();
	void initialize();
	~Game();
private:
	sf::RenderWindow _window;
	sf::View _view;
	FPSCounter _fps;
};

