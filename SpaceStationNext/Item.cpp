#include "Item.h"

ItemFactory::map_type* ItemFactory::map = NULL;

Item::Item()
{
}


Item::~Item()
{
}

void Item::setId(unsigned int id)
{
	_id = id;
}

unsigned int Item::getId() const
{
	return _id;
}

void Item::setName(string & name)
{
	_name = name;
}

const string & Item::getName() const
{
	return _name;
}

void Item::setDesc(string & desc)
{
	_description = desc;
}

const string & Item::getDesc() const
{
	return _description;
}

void Item::update()
{
}
