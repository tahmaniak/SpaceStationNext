#pragma once
#include "rapidjson\document.h"
#include <map>

using namespace std;
using namespace rapidjson;

class Item
{
public:
	Item();

	void setId(unsigned int id);
	unsigned int getId() const;
	void setName(string& name);
	const string& getName() const;
	void setDesc(string& desc);
	const string& getDesc() const;

	virtual void update();

	~Item();
protected:

private:
	unsigned int _id;
	string _name;
	string _description;
};

template<typename T> Item * createItem() { return new T; }

struct ItemFactory {
	typedef std::map<std::string, Item*(*)()> map_type;

	static Item * createInstance(std::string const& s) {
		map_type::iterator it = getMap()->find(s);
		if (it == getMap()->end())
			return 0;
		return it->second();
	}

protected:
	static map_type * getMap() {
		if (!map) { map = new map_type; }
		return map;
	}

private:
	static map_type * map;
};

template<typename T>
struct ItemRegister : ItemFactory {
	ItemRegister(std::string const& s) {
		getMap()->insert(std::make_pair(s, &createItem<T>));
	}
};