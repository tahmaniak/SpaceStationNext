#include "Itemmanager.h"


void Itemmanager::initialize()
{
	ifstream file("data/items/items.json");

	stringstream buffer;
	buffer << file.rdbuf();

	Document itemDoc;
	itemDoc.Parse(buffer.str().c_str());

	if (itemDoc.GetParseError() != ParseErrorCode::kParseErrorNone) {
		throw exception(("JSON parse error " + to_string(itemDoc.GetParseError()) + " at offset " + to_string(itemDoc.GetErrorOffset())).c_str());
	}

	cout << itemDoc.Size() << " item entries found in items.json" << endl;

	for (int i = 0; i < itemDoc.Size(); i++) {
		string type = itemDoc[i]["type"].GetString();
		string name = itemDoc[i]["name"].GetString();
		unsigned int id = itemDoc[i]["id"].GetUint();

		auto_ptr<Item> newItem(ItemFactory::createInstance(itemDoc[i]["type"].GetString()));
		newItem->setId(id);
		newItem->setName(name);

		if (newItem.get() == nullptr) {
			throw exception(("Invalid item type '"+ type +"' for item '"+ name + "'["+ to_string(id) +"]").c_str());
		}

		StaticItem* statItem = dynamic_cast<StaticItem*>(newItem.get());
		if (statItem != nullptr) {
			statItem->setMapTex(itemDoc[i]["maptex"].GetUint());
		}

		_items[id] = newItem;
	}
}


Itemmanager::~Itemmanager()
{
}

const Item * Itemmanager::getItem(unsigned int id) const
{
	return _items.at(id).get();
}
