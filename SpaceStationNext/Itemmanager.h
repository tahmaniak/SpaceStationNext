#pragma once
#include "Item.h"
#include "StaticItem.h"
#include "Floor.h"
#include <map>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "rapidjson\document.h"

using namespace std;
using namespace rapidjson;

class Itemmanager
{
public:
	void initialize();
	static Itemmanager& getInstance()
	{
		static Itemmanager instance;
							  
		return instance;
	}
	~Itemmanager();

	const Item* getItem(unsigned int id) const;

	Itemmanager(Itemmanager const&) = delete;
	void operator=(Itemmanager const&) = delete;
private:
	Itemmanager() {}

	map<unsigned int, auto_ptr<Item>> _items;
};

