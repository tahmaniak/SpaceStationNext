#pragma once
#include "Item.h"
class StaticItem :
	public Item
{
public:
	StaticItem();
	void setMapTex(unsigned int mapTex);
	unsigned int getMapTex() const;
	~StaticItem();
private:
	unsigned int _mapTex;
};

