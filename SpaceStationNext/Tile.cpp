#include "Tile.h"



Tile::Tile()
{
	_wall = nullptr;
	_floor = nullptr;
	_baseFloor = false;
}

const Floor* Tile::setFloor(unsigned int id)
{
	if (id == 0) {
		return _floor = nullptr;
	}

	const Item* item = Itemmanager::getInstance().getItem(id);

	if (item == nullptr) {
		throw exception("Unknown item: " + id);
	}

	_floor = dynamic_cast<const Floor*>(item);

	if (_floor == nullptr) {
		throw exception("Item is not a floor: " + id);
	}

	return _floor;
}

const Wall* Tile::setWall(unsigned int id)
{
	if (id == 0) {
		return _wall = nullptr;
	}

	const Item* item = Itemmanager::getInstance().getItem(id);

	if (item == nullptr) {
		throw exception("Unknown item: " + id);
	}

	_wall = dynamic_cast<const Wall*>(item);

	if (_wall == nullptr) {
		throw exception("Item is not a wall: " + id);
	}

	return _wall;
}


Tile::~Tile()
{
}
