#pragma once
#include <iostream>
#include "DynamicItem.h"
#include "Floor.h"
#include "Wall.h"
#include "Itemmanager.h"

using namespace std;

class Tile
{
public:
	Tile();
	const Floor* setFloor(unsigned int id);
	const Wall* setWall(unsigned int id);
	const Floor* getFloor() const { return _floor; };
	const Wall* getWall() const { return _wall; };;
	~Tile();
private:
	const Wall* _wall;
	const Floor* _floor;
	bool _baseFloor;
	vector<DynamicItem> _dynItems;
};

