#pragma once
#include "StaticItem.h"

class Wall : public StaticItem
{
public:
	Wall();
	static ItemRegister<Wall> reg;
	~Wall();
};

