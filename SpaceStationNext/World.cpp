#include "World.h"



World::World()
{
	_vertices.setPrimitiveType(sf::PrimitiveType::Quads);
	unload();
}

void World::setView(const sf::View & view)
{
	_view = &view;
	_viewTileSize = ceil(view.getSize().x / TILE_SIZE) + 1;
	_vertices.resize(_viewTileSize*_viewTileSize*4);
	_viewTileCoords = sf::Vector2i(floor((_view->getCenter().x*2 - _view->getSize().x) / 2 / 32), 
								   floor((_view->getCenter().y*2 - _view->getSize().y) / 2 / 32));
	cout << "View tile coords: " << _viewTileCoords.x << ";" << _viewTileCoords.y << endl;
}

void World::load(string& name)
{
	assert(_view != nullptr);

	unload();

	if (!_tex.loadFromFile("data/maps/" + name + "/tex.png")) {
		cout << "Map texture not found for map \"" << name << "\"" << endl;
		if (!_tex.loadFromFile("data/maps/default/tex.png")) {
			throw exception("Default map texture not found!");
		}
	}

	if (_tex.getSize() != sf::Vector2u(1024, 1024)) {
		throw exception("Wrong map texture size!");
	}

	ifstream file("data/maps/" + name + "/tiles.json");

	stringstream buffer;
	buffer << file.rdbuf();

	Document mapDoc;
	mapDoc.Parse(buffer.str().c_str());

	if (mapDoc.GetParseError() != ParseErrorCode::kParseErrorNone) {
		throw exception(("JSON parse error " + to_string(mapDoc.GetParseError()) + " at offset " + to_string(mapDoc.GetErrorOffset())).c_str());
	}

	_map.size.x = mapDoc["size"]["x"].GetUint();
	_map.size.y = mapDoc["size"]["x"].GetUint();

	GenericArray<false, GenericValue<UTF8<>>> floorData = mapDoc["floor"].GetArray();
	GenericArray<false, GenericValue<UTF8<>>> wallData = mapDoc["floor"].GetArray();

	_tiles = new Tile*[_map.size.x];

	for (int i = 0; i < _map.size.x; i++) {
		_tiles[i] = new Tile[_map.size.y];
	}

	for (int y = 0; y < _map.size.y; y++) {
		for (int x = 0; x < _map.size.x; x++) {
			unsigned int itemId = floorData[y*_map.size.y+x].GetUint();
			if (itemId != 0) {
				setFloor(sf::Vector2u(x,y),itemId);
			}
			itemId = wallData[y*_map.size.y + x].GetUint();
			if (itemId != 0) {
				setWall(sf::Vector2u(x, y), itemId);
			}
		}
	}

	_map.name = name;
}

void World::unload()
{
	for (int i = 0; i < _map.size.y; i++) {
		delete[] _tiles[i];
	}
	delete[] _tiles;

	_map.size = sf::Vector2u(0, 0);
	_map.name = "";
}

void World::update()
{
	sf::Vector2i oldViewTileCoods = _viewTileCoords;
	_viewTileCoords = sf::Vector2i(floor((_view->getCenter().x * 2 - _view->getSize().x) / 2 / 32),
								   floor((_view->getCenter().y * 2 - _view->getSize().y) / 2 / 32));
	if (oldViewTileCoods != _viewTileCoords) {
		cout << "View tile coords: " << _viewTileCoords.x << ";" << _viewTileCoords.y << endl;
		for (int x = 0; x < _viewTileSize; x++) {
			for (int y = 0; y < _viewTileSize; y++) {
				sf::Vector2u coords(x,y);
				updateVertices(coords);
			}
		}
	}
}

void World::setFloor(sf::Vector2u coords, unsigned int id)
{
	_tiles[coords.x][coords.y].setFloor(id);

	if (isMapTileInView(coords)) {
		updateVertices(mapCoordsToViewCoords(coords));
	}
}

void World::setWall(sf::Vector2u coords, unsigned int id)
{
	_tiles[coords.x][coords.y].setWall(id);

	if (isMapTileInView(coords)) {
		updateVertices(mapCoordsToViewCoords(coords));
	}
	
}

bool World::isMapTileInView(sf::Vector2u mapcoords)
{
	
	if ((int)mapcoords.x >= _viewTileCoords.x && (int)mapcoords.x < _viewTileCoords.x + _viewTileSize &&
		(int)mapcoords.y >= _viewTileCoords.y && (int)mapcoords.y < _viewTileCoords.y + _viewTileSize) {
		return true;
	}

	return false;
}

bool World::isViewTileInMap(sf::Vector2u tilecoords)
{
	if (tilecoords.x + _viewTileCoords.x < _map.size.x && tilecoords.x + _viewTileCoords.x >= 0 &&
		tilecoords.y + _viewTileCoords.y < _map.size.y && tilecoords.y + _viewTileCoords.y >= 0) {
		return true;
	}
	return false;
}

const bool World::getLoaded() const
{
	return _map.name == "";
}

const sf::Vector2i & World::getSize() const
{
	return _size;
}

World::~World()
{
	unload();
}

void World::updateVertices(sf::Vector2u coords)
{
	unsigned int quadIndex = (coords.y * _viewTileSize + coords.x) * 4;

	if (!isViewTileInMap(coords)) {
		//view coord is not in map
		for (int i = 0; i < 4; i++) {
			_vertices[quadIndex + i] = sf::Vertex();
		}
		return;
	}

	sf::Vector2u mapcoords = viewCoordsToMapCoords(coords);

	const StaticItem* item = nullptr;
	if ((item = _tiles[mapcoords.x][mapcoords.y].getWall()) == nullptr) {
		if ((item = _tiles[mapcoords.x][mapcoords.y].getFloor()) == nullptr) {
			//no floor nor wall at map coords
			for (int i = 0; i < 4; i++) {
				_vertices[quadIndex+i] = sf::Vertex();
			}
			return;
		}
	}

	unsigned int maptexindex = item->getMapTex();
	sf::Vector2u maptexcoords((maptexindex % 32)*TILE_SIZE, std::floor(maptexindex / 32)*TILE_SIZE);
	

	_vertices[quadIndex].position = sf::Vector2f((_viewTileCoords.x*TILE_SIZE )+coords.x*TILE_SIZE, (_viewTileCoords.y*TILE_SIZE) +coords.y*TILE_SIZE);
	_vertices[quadIndex + 1].position = sf::Vector2f((_viewTileCoords.x*TILE_SIZE) + coords.x*TILE_SIZE + TILE_SIZE, (_viewTileCoords.y*TILE_SIZE) + coords.y*TILE_SIZE);
	_vertices[quadIndex + 2].position = sf::Vector2f((_viewTileCoords.x*TILE_SIZE) + coords.x*TILE_SIZE + TILE_SIZE, (_viewTileCoords.y*TILE_SIZE) + coords.y*TILE_SIZE + TILE_SIZE);
	_vertices[quadIndex + 3].position = sf::Vector2f((_viewTileCoords.x*TILE_SIZE) + coords.x*TILE_SIZE, (_viewTileCoords.y*TILE_SIZE) + coords.y*TILE_SIZE + TILE_SIZE);

	_vertices[quadIndex].texCoords = sf::Vector2f(maptexcoords.x, maptexcoords.y);
	_vertices[quadIndex + 1].texCoords = sf::Vector2f(maptexcoords.x + TILE_SIZE, maptexcoords.y);
	_vertices[quadIndex + 2].texCoords = sf::Vector2f(maptexcoords.x + TILE_SIZE, maptexcoords.y + TILE_SIZE);
	_vertices[quadIndex + 3].texCoords = sf::Vector2f(maptexcoords.x, maptexcoords.y + TILE_SIZE);
	
	unsigned int lighting = 255;

	_vertices[quadIndex].color = sf::Color(lighting, lighting, lighting, 255);
	_vertices[quadIndex + 1].color = sf::Color(lighting, lighting, lighting, 255);
	_vertices[quadIndex + 2].color = sf::Color(lighting, lighting, lighting, 255);
	_vertices[quadIndex + 3].color = sf::Color(lighting, lighting, lighting, 255);
}

void World::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
	rs.texture = &_tex;
	rt.draw(_vertices, rs);
}

sf::Vector2u World::mapCoordsToViewCoords(sf::Vector2u mapcoords)
{
	assert(isMapTileInView(mapcoords));
	return sf::Vector2u(mapcoords.x - _viewTileCoords.x, mapcoords.y - _viewTileCoords.y);
}

sf::Vector2u World::viewCoordsToMapCoords(sf::Vector2u viewcoords)
{
	assert(isViewTileInMap(viewcoords));
	return sf::Vector2u(viewcoords.x + _viewTileCoords.x, viewcoords.y + _viewTileCoords.y);
}
