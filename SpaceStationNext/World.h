#pragma once
#include "SFML\Graphics.hpp"
#include <vector>
#include "Tile.h"
#include "rapidjson\document.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;
using namespace rapidjson;


#define TILE_SIZE 32

struct MapInfo {
	string name;
	sf::Vector2u size; //size in tiles
};

class World : public sf::Drawable
{
public:
	World();

	static World& getInstance()
	{
		static World instance;
		return instance;
	}

	World(World const&) = delete;
	void operator=(World const&) = delete;

	//Sets the window view for drawing and managing the vertexarray. You have to set the view before doing anything else.
	void setView(const sf::View& view);

	//Loads a map
	void load(string& name);
	void unload();

	void update();

	void setFloor(sf::Vector2u coords, unsigned int id);
	void setWall(sf::Vector2u coords, unsigned int id);

	bool isMapTileInView(sf::Vector2u mapcoords);
	bool isViewTileInMap(sf::Vector2u tilecoords);

	const bool getLoaded() const;
	const sf::Vector2i& getSize() const;

	~World();
private:
	void updateVertices(sf::Vector2u coords);
	virtual void draw(sf::RenderTarget& rt, sf::RenderStates rs) const;
	sf::VertexArray _vertices;
	Tile** _tiles;
	sf::Vector2i _size;
	sf::Texture _tex;
	MapInfo _map;
	const sf::View* _view;										//Pointer to the window view
	unsigned int _viewTileSize;									//Size in tiles of the view area
	sf::Vector2i _viewTileCoords;								//Map tile coordinates of the view area
	sf::Vector2u mapCoordsToViewCoords(sf::Vector2u mapcoords);
	sf::Vector2u viewCoordsToMapCoords(sf::Vector2u viewcoords);
};

